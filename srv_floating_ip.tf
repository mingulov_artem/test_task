# -*- coding: utf-8 -*-

data "openstack_networking_network_v2" "external_network" {
  name   = "external-network"
  region = "${var.region}"
}

resource "openstack_networking_router_v2" "router_1" {
  region              = "${var.region}"
  name                = "Router"
  admin_state_up      = "true"
  external_network_id = "${data.openstack_networking_network_v2.external_network.id}"
}

resource "openstack_networking_network_v2" "nat_network" {
  region         = "${var.region}"
  name           = "nat-network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_1" {
  name            = "192.168.0.0/24"
  region          = "${var.region}"
  network_id      = "${openstack_networking_network_v2.nat_network.id}"
  enable_dhcp     = "false"
  dns_nameservers = ["188.93.16.19", "188.93.17.19"]
  gateway_ip      = "192.168.0.1"
  cidr            = "192.168.0.0/24"
  ip_version      = 4
}

resource "openstack_networking_router_interface_v2" "router_interface_1" {
  region    = "${var.region}"
  router_id = "${openstack_networking_router_v2.router_1.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_1.id}"
}

resource "openstack_blockstorage_volume_v2" "disk_for_sandbox01" {
  name        = "disk_for_sandbox01"
  region      = "${var.region}"
  size        = "${var.size_disk}"
  image_id    = "dbe5a6a9-efd5-4993-b030-fd1c48480c7a"
  volume_type = "fast.${var.zone}"
}

resource "openstack_compute_instance_v2" "sandbox01" {
  name              = "sandbox01"
  flavor_name       = "${var.flavor_name}"
  region            = "${var.region}"
  availability_zone = "${var.zone}"
  key_pair          = "${var.key_pair}"

  network {
    uuid        = "${openstack_networking_subnet_v2.subnet_1.network_id}"
    fixed_ip_v4 = "192.168.0.4"
  }

  metadata = {
    "x_sel_server_default_addr" = "{\"ipv4\":\"\"}"
  }

  block_device {
    uuid             = "${openstack_blockstorage_volume_v2.disk_for_sandbox01.id}"
    source_type      = "volume"
    boot_index       = 0
    destination_type = "volume"
  }
}

resource "openstack_networking_floatingip_v2" "floating_ip_01" {
  pool   = "external-network"
  region = "${var.region}"
}

resource "openstack_compute_floatingip_associate_v2" "floating_ip_01" {
  floating_ip = "${openstack_networking_floatingip_v2.floating_ip_01.address}"
  instance_id = "${openstack_compute_instance_v2.sandbox01.id}"
  fixed_ip    = "${openstack_compute_instance_v2.sandbox01.network.0.fixed_ip_v4}"
  region      = "${var.region}"
}

output "instance_ip" {
  value = "${openstack_compute_floatingip_associate_v2.floating_ip_01.floating_ip}"
}
