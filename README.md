# Подготовка

1. [Установить **Terraform**](https://learn.hashicorp.com/terraform/getting-started/install.html).

2. [Установить **Ansible**](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

3. [Получить доступ к API **OpenStack**](https://kb.selectel.ru/22060427.html).

# Развертывание

## Создание сервера

1. Установить плагины для **Terraform** командой `terraform init` .

2. Подготовить окружение экспортировав переменные, необходимые для работы клиента, выполнив команду
`source rc.sh` .

3. Создать план исполнения командой `terraform plan`  .

4. Применить полученный план командой `terraform apply` .

## Конфигурирование сервера

1. Получить IP-адрес сервера, выполнив команду `terraform output instance_ip`, и добавить его в файл *host* в секцию **py3-hosts** .

2. Установить необходимое программное обеспечение и запустить окружение командой `ansible-playbook playbook.yaml` .

## ToDo

1. Запуск исполнения сценария **Ansible** из **Terraform**.

