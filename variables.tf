# -*- coding: utf-8 -*-

variable "project_id" {
  type        = "string"
  description = "Project ID"
}

variable "region" {
  type    = "string"
  default = "ru-3"
}

variable "zone" {
  type    = "string"
  default = "ru-3a"
}

variable "size_disk" {
  type    = "string"
  default = "5"
}

variable "image_id" {
  type    = "string"
  default = "dbe5a6a9-efd5-4993-b030-fd1c48480c7a"
}

variable "flavor_name" {
  type    = "string"
  default = "BL1.1-2048"
}

variable "key_pair" {
  type    = "string"
  default = "mingulov"
}
